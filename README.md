# symfony-translation

Tools to internationalize your application. https://packagist.org/packages/symfony/translation

# Official documentation
* [*Translations*](https://symfony.com/doc/current/translation.html)

# Unofficial documentation
* [*Nuts & bolts of internationalization and localization with Symfony*
  ](https://blog.undabot.com/nuts-bolts-of-internationalization-and-localization-with-symfony-c06cd3ba5697?gi=f9f143fee1fd)
  2022-03 Mateo Fuzul
* [*Multilingual Symfony API*
  ](https://medium.com/q-software/multilingual-symfony-api-b31fa5f204fe)
  2019-08 Bruno Krnetic
